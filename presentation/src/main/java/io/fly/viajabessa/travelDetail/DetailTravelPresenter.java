package io.fly.viajabessa.travelDetail;

import io.fly.data.TravelRepository;
import io.fly.model.Travel;
import io.fly.viajabessa.base.BasePresenter;
import rx.Scheduler;

/**
 * Created by medal on 4/6/2018.
 */

public class DetailTravelPresenter extends BasePresenter<DetailTravelContract.View> implements DetailTravelContract.Presenter {

    private final Scheduler mainScheduler, ioScheduler;
    private TravelRepository travelRepository;

    DetailTravelPresenter(TravelRepository travelRepository, Scheduler ioScheduler, Scheduler mainScheduler) {
        this.travelRepository = travelRepository;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
    }

    @Override
    public void showTravel(Travel travel) {
        if(travel!=null) {
            getView().showTravel(travel);
        } else {
            throw new ModelNull();
        }

    }

    public static class ModelNull extends RuntimeException {
        public ModelNull() {
            super("Please pass a valid model");
        }
    }

}
