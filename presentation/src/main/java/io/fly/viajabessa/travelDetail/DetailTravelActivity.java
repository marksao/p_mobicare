package io.fly.viajabessa.travelDetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.android.airmapview.AirMapMarker;
import com.airbnb.android.airmapview.AirMapView;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import io.fly.injection.InjectionMock;
import io.fly.model.Travel;
import io.fly.viajabessa.R;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DetailTravelActivity extends AppCompatActivity implements DetailTravelContract.View {

    private TextView tv_price;
    private TextView tv_title;
    private TextView tv_description;
    private ImageView travel_detail_img;
    private DetailTravelPresenter presenter;
    private AirMapView map;
    private Toolbar toolbar;

    private Travel travel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_travel);

        presenter = new DetailTravelPresenter(InjectionMock.provideListTravels(), Schedulers.io(), AndroidSchedulers.mainThread());
        presenter.attachView(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        tv_description = findViewById(R.id.tv_description);
        tv_price = findViewById(R.id.tv_price);
        tv_title = findViewById(R.id.tv_title);
        travel_detail_img = findViewById(R.id.travel_detail_img);

        map = findViewById(R.id.map);
        map.initialize(getSupportFragmentManager());

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle!=null){
            travel = (Travel) bundle.getSerializable("travel");
        }

        presenter.showTravel(travel);
    }

    @Override
    public void showTravel(Travel travel) {

        Picasso.get().load(travel.getImage()).into(travel_detail_img);

        tv_title.setText(String.format("%s",travel.getTitle()));
        tv_price.setText(String.format("R$ %s",travel.getPrice()));
        tv_description.setText(String.format("%s",travel.getDescribedPackage()));

        map.addMarker(new AirMapMarker.Builder()
                .id(1)
                .position(new LatLng(37.78443, -122.40805))
                .title(travel.getTitle())
                .iconId(R.drawable.ic_location_on_red_600_24dp)
                .build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
