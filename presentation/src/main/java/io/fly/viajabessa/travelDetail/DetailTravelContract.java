package io.fly.viajabessa.travelDetail;

import io.fly.model.Travel;
import io.fly.viajabessa.base.MvpPresenter;
import io.fly.viajabessa.base.MvpView;

/**
 * Created by medal on 4/6/2018.
 */

public interface DetailTravelContract {
    interface View extends MvpView {
        void showTravel(Travel travel);
    }

    interface Presenter extends MvpPresenter<DetailTravelContract.View> {
        void showTravel(Travel travel);
    }
}
