package io.fly.viajabessa.base;

/**
 * Created by medal on 4/6/2018.
 */

public interface MvpPresenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();

}
