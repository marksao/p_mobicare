package io.fly.viajabessa.travelList;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import io.fly.viajabessa.R;


/**
 * Created by medal on 4/6/2018.
 */

public class ListTravelViewHolder extends RecyclerView.ViewHolder {

    ImageView iv_img_travel_intro;
    TextView tv_travel_title;
    TextView tv_travel_value;
    LinearLayout linear_layout;
    ProgressBar progressbar;
    public ListTravelViewHolder(View itemView) {
        super(itemView);
        iv_img_travel_intro = itemView.findViewById(R.id.travel_intro_img);
        tv_travel_title = itemView.findViewById(R.id.travel_title);
        tv_travel_value = itemView.findViewById(R.id.travel_value);
        linear_layout = itemView.findViewById(R.id.linear_layout);
        progressbar = itemView.findViewById(R.id.progressbar);
    }
}
