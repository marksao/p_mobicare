package io.fly.viajabessa.travelList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.fly.model.Travel;
import io.fly.viajabessa.R;

/**
 * Created by medal on 4/6/2018.
 */

public class ListTravelAdapter extends RecyclerView.Adapter<ListTravelViewHolder> {

    private List<Travel> _list;
    private Context context;
    private ListTravelContract.AdapterAssistance adapterAssistance;

    public ListTravelAdapter(List<Travel> list) {
        this._list = list;
    }

    @NonNull
    @Override
    public ListTravelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_travel_list, parent, false);
        context = parent.getContext();
        return new ListTravelViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListTravelViewHolder holder, int position) {
        final Travel travel = _list.get(position);

        holder.tv_travel_value.setText(String.format("R$ %s",travel.getPrice()));
        holder.tv_travel_title.setText(String.format("%s",travel.getTitle()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterAssistance.navigateTo(travel,holder.iv_img_travel_intro);
            }
        });

        Picasso.get().load(travel.getImage()+"?aki_policy=small").into(holder.iv_img_travel_intro, new Callback() {
            @Override
            public void onSuccess() {
                holder.progressbar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                Log.e("ExceptionImage",e.getMessage());
                Toast.makeText(context, "Erro ao carregar imagem", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (_list != null) ? _list.size() : 0;
    }

    public void setAdapterAssistance(ListTravelContract.AdapterAssistance adapterAssistance) {
        this.adapterAssistance = adapterAssistance;
    }

    void setItems(List<Travel> _list) {
        this._list = _list;
        this.notifyDataSetChanged();
    }
}
