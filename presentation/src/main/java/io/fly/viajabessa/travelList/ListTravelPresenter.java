package io.fly.viajabessa.travelList;


import java.util.List;

import io.fly.data.TravelRepository;
import io.fly.model.Device;
import io.fly.model.Travel;
import io.fly.viajabessa.base.BasePresenter;
import rx.Scheduler;
import rx.Subscriber;

/**
 * Created by medal on 4/6/2018.
 */

public class ListTravelPresenter extends BasePresenter<ListTravelContract.View> implements ListTravelContract.Presenter {

    private final Scheduler mainScheduler, ioScheduler;
    private TravelRepository travelRepository;

    ListTravelPresenter(TravelRepository travelRepository, Scheduler ioScheduler, Scheduler mainScheduler) {
        this.travelRepository = travelRepository;
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
    }

    @Override
    public void listTravels(Device device) {
        checkViewAttached();
        getView().showLoading();

        addSubscription(travelRepository.listTravels(device)
            .subscribeOn(ioScheduler)
            .observeOn(mainScheduler)
            .subscribe(new Subscriber<List<Travel>>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    getView().hideLoading();
                    getView().showError(e.getMessage());
                }

                @Override
                public void onNext(List<Travel> travelList) {
                    getView().hideLoading();
                    getView().showResults(travelList);
                }
            }));
    }

}
