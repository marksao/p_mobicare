package io.fly.viajabessa.travelList;

import android.widget.ImageView;

import java.util.List;

import io.fly.model.Device;
import io.fly.model.Travel;
import io.fly.viajabessa.base.MvpPresenter;
import io.fly.viajabessa.base.MvpView;

/**
 * p_mobicare
 * Created by medal on 4/6/2018 at 06.
 */

public interface ListTravelContract {
    interface View extends MvpView {
        void showResults(List<Travel> travelList);

        void showError(String message);

        void showLoading();

        void hideLoading();
    }

    interface Presenter extends MvpPresenter<View> {
        void listTravels(Device device);
    }

    interface AdapterAssistance {
        void navigateTo(Travel travel, ImageView view);
    }
}
