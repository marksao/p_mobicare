package io.fly.viajabessa.travelList;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import io.fly.injection.InjectionMock;
import io.fly.model.Device;
import io.fly.model.Travel;
import io.fly.viajabessa.R;
import io.fly.viajabessa.travelDetail.DetailTravelActivity;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ListTravelActivity extends AppCompatActivity implements ListTravelContract.AdapterAssistance, ListTravelContract.View {

    private ListTravelAdapter listTravelAdapter;
    private ListTravelPresenter listTravelPresenter;
    private ProgressBar progressBar;
    private RecyclerView rvListTravel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_travel);

        listTravelPresenter = new ListTravelPresenter(InjectionMock.provideListTravels(), Schedulers.io(), AndroidSchedulers.mainThread());
        listTravelPresenter.attachView(this);

        progressBar = findViewById(R.id.progressbar);
        rvListTravel = findViewById(R.id.rvListTravel);

        listTravelAdapter = new ListTravelAdapter(null);
        listTravelAdapter.setAdapterAssistance(this);
        GridLayoutManager gm = new GridLayoutManager(this,2);
        rvListTravel.setLayoutManager(gm);
        rvListTravel.setAdapter(listTravelAdapter);

        Device device = Device.getDevice();
        if(device.getBrand()==null||device.getBrand().isEmpty()){
            device.setModel(Build.MODEL);
            device.setBrand(Build.MANUFACTURER);
            device.setVersion(String.format("%s",Build.VERSION.SDK_INT));
        }

        listTravelPresenter.listTravels(device);
    }

    @Override
    public void navigateTo(Travel travel, ImageView view) {
        Intent intent = new Intent(this, DetailTravelActivity.class);
        intent.putExtra("travel",travel);
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, (View) view, "trans");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startActivity(intent, options.toBundle());
        }else{
            startActivity(intent);
        }
    }

    @Override
    public void showResults(List<Travel> travelList) {
        rvListTravel.setVisibility(View.VISIBLE);
        listTravelAdapter.setItems(travelList);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, "Error on receiving data: " + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        rvListTravel.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        rvListTravel.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listTravelPresenter.detachView();
    }
}
