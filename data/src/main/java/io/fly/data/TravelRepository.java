package io.fly.data;



import java.util.List;

import io.fly.model.Device;
import io.fly.model.Travel;
import rx.Observable;

/**
 * Created by medal on 4/5/2018.
 */

public interface TravelRepository {
    Observable<List<Travel>> listTravels(Device device);
}
