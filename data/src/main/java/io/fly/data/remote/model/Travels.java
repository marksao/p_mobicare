package io.fly.data.remote.model;

import java.util.List;

import io.fly.model.Travel;

/**
 * Created by medal on 4/5/2018.
 */

public class Travels {

    private List<Travel> travels;

    public Travels() {
    }

    public Travels(List<Travel> travels) {
        this.travels = travels;
    }

    public List<Travel> getTravels() {
        return travels;
    }

    public void setTravels(List<Travel> travels) {
        this.travels = travels;
    }
}
