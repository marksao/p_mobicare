package io.fly.data.remote;


import io.fly.data.remote.model.Travels;
import io.fly.model.Device;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by medal on 4/5/2018.
 */

public interface TravelRestService {
    @POST("listTravel")
    Observable<Travels> listTravels(@Body Device device);
}
