package io.fly.data.remote;


import java.util.ArrayList;
import java.util.List;

import io.fly.data.remote.model.Travels;
import io.fly.model.Device;
import io.fly.model.Travel;
import rx.Observable;

/**
 * Created by medal on 4/5/2018.
 */

public class MockTravelRestServiceImpl implements TravelRestService {

    private Travels travels = new Travels();
    private static Observable listTravelResult = null;

    public MockTravelRestServiceImpl() {
        List<Travel> travelMock = new ArrayList<>();
        travelMock.add(travel1());
        travelMock.add(travel2());
        travelMock.add(travel3());
        travelMock.add(travel4());
        travelMock.add(travel5());
        travelMock.add(travel6());
        travelMock.add(travel7());
        travelMock.add(travel8());
        travels.setTravels(travelMock);
    }

    @Override
    public Observable<Travels> listTravels(Device device) {
        return (listTravelResult!=null) ? listTravelResult : Observable.just(travels);
    }

    private Travel travel1() {
        return new Travel("PackageName 1",
                "1.400,00",
                "https://a0.muscache.com/im/pictures/23884483/91d67209_original.jpg",
                "random_location_pic",
                "describedPackage 1");
    }

    private Travel travel2() {
        return new Travel("PackageName 2",
                "2.400,00",
                "https://a0.muscache.com/im/pictures/0e33d6aa-efd7-4213-be92-c74388faa0a1.jpg",
                "random_location_pic",
                "describedPackage 2");
    }

    private Travel travel3() {
        return new Travel("PackageName 3",
                "3.400,00",
                "https://a0.muscache.com/im/pictures/881d76c6-4de6-4808-9f0b-36462b6c48a2.jpg",
                "random_location_pic",
                "describedPackage 3");
    }

    private Travel travel4() {
        return new Travel("PackageName 4",
                "4.400,00",
                "https://a0.muscache.com/im/pictures/717fef15-755d-4d52-b9c4-ed12e2300db5.jpg",
                "random_location_pic",
                "describedPackage 4");
    }

    private Travel travel5() {
        return new Travel("PackageName 5",
                "5.400,00",
                "https://a0.muscache.com/im/pictures/717fef15-755d-4d52-b9c4-ed12e2300db5.jpg",
                "random_location_pic",
                "describedPackage 5");
    }

    private Travel travel6() {
        return new Travel("PackageName 6",
                "6.400,00",
                "https://a0.muscache.com/im/pictures/717fef15-755d-4d52-b9c4-ed12e2300db5.jpg",
                "random_location_pic",
                "describedPackage 6");
    }

    private Travel travel7() {
        return new Travel("PackageName 7",
                "7.400,00",
                "https://a0.muscache.com/im/pictures/717fef15-755d-4d52-b9c4-ed12e2300db5.jpg",
                "random_location_pic",
                "describedPackage 7");
    }

    private Travel travel8() {
        return new Travel("PackageName 8",
                "8.400,00",
                "https://a0.muscache.com/im/pictures/717fef15-755d-4d52-b9c4-ed12e2300db5.jpg",
                "random_location_pic",
                "describedPackage 8");
    }
}
