package io.fly.data;

import java.io.IOException;
import java.util.List;

import io.fly.data.remote.TravelRestService;
import io.fly.model.Device;
import io.fly.model.Travel;
import rx.Observable;

/**
 * Created by medal on 4/5/2018.
 */

public class TravelRepositoryImpl implements TravelRepository {

    private TravelRestService travelRestService;

    public TravelRepositoryImpl(TravelRestService travelRestService) {
        this.travelRestService = travelRestService;
    }

    @Override
    public Observable<List<Travel>> listTravels(Device device) {
        return Observable.defer(() -> travelRestService.listTravels(device).concatMap(
                call -> Observable.from(call.getTravels()).toList()))
                .retryWhen(observable -> observable.flatMap(o -> {
                    if (o instanceof IOException) {
                        return Observable.just(null);
                    }
                    return Observable.error(o);
                }));
    }
}
