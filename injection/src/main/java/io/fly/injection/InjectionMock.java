package io.fly.injection;

import io.fly.data.TravelRepository;
import io.fly.data.TravelRepositoryImpl;
import io.fly.data.remote.MockTravelRestServiceImpl;
import io.fly.data.remote.TravelRestService;

/**
 * Created by medal on 4/6/2018.
 */

public class InjectionMock {

    private static TravelRestService travelRestService;

    public static TravelRepository provideListTravels() {
        return new TravelRepositoryImpl(provideTravelsRestService());
    }

    static TravelRestService provideTravelsRestService() {
        if (travelRestService == null) {
            travelRestService = new MockTravelRestServiceImpl();
        }
        return travelRestService;
    }

}
