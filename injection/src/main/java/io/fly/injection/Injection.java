package io.fly.injection;

import io.fly.data.TravelRepository;
import io.fly.data.TravelRepositoryImpl;
import io.fly.data.remote.TravelRestService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by medal on 4/6/2018.
 */

public class Injection {

    private final static String BASE_URL = "Alguma url para o serviço";
    private static OkHttpClient okHttpClient;
    private static TravelRestService travelRestService;
    private static Retrofit retrofitInstance;

    public static TravelRepository provideListApps() {
        return new TravelRepositoryImpl(provideTravelsRepo());
    }

    static TravelRestService provideTravelsRepo() {
        if (travelRestService == null) {
            travelRestService = getRetrofitInstance().create(TravelRestService.class);
        }
        return travelRestService;
    }

    static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
            okHttpClient = new OkHttpClient.Builder().addInterceptor(logging).build();
        }

        return okHttpClient;
    }

    static Retrofit getRetrofitInstance() {
        if (retrofitInstance == null) {
            Retrofit.Builder retrofit = new Retrofit.Builder().client(Injection.getOkHttpClient()).baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
            retrofitInstance = retrofit.build();

        }
        return retrofitInstance;
    }

}
