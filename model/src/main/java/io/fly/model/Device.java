package io.fly.model;

/**
 * Created by medal on 4/5/2018.
 */

public class Device {

    private String version;
    private String model;
    private String brand;

    private static Device device;

    public static Device getDevice(){
        return (device != null) ? device : new Device();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
