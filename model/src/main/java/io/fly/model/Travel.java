package io.fly.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by medal on 4/5/2018.
 */

public class Travel implements Serializable {

    @SerializedName("PackageName")
    @Expose
    private String title;//PackageName
    @SerializedName("Price")
    @Expose
    private String price;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("LatLong")
    @Expose
    private String localizationPicture;
    @SerializedName("DescribedPackage")
    @Expose
    private String describedPackage;

    public Travel() {
    }

    public Travel(String title, String price, String image, String localizationPicture, String describedPackage) {
        this.title = title;
        this.price = price;
        this.image = image;
        this.localizationPicture = localizationPicture;
        this.describedPackage = describedPackage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLocalizationPicture() {
        return localizationPicture;
    }

    public void setLocalizationPicture(String localizationPicture) {
        this.localizationPicture = localizationPicture;
    }

    public String getDescribedPackage() {
        return describedPackage;
    }

    public void setDescribedPackage(String describedPackage) {
        this.describedPackage = describedPackage;
    }
}
